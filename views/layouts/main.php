<!doctype html>
<html lang="en">
<?php
require(__DIR__ . '/../header.php');
?>
<body>
<div class="container">
    {{content}}
</div>
<?php
require(__DIR__ . '/../footer.php');
?>
</body>
</html>