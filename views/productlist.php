<?php
/** @var $model \app\models\ProductListForm */

use app\core\form\Form;

$this->title = 'Product List';
?>
<div class="row mt-3">
    <div class="col-12">
        <div class="row align-items-center mx-1">
            <div class="col">
                <h1><?= $this->title ?></h1>
            </div>
            <div class="col-auto">
                <div class="buttons">
                    <a href="/productadd" class="d-inline-block">
                        <button class="btn btn-primary">ADD</button>
                    </a>
                    <button type="submit" id="delete-product-button" form="form" class="btn btn-danger">MASS DELETE
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <hr>
    </div>
    <div class="col-12">
        <form action="" method="POST" id="form">
            <div class="row">
                <?php
                $products = $model->getProducts();
                foreach ($products as $product): ?>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-12 mb-5">
                        <div class="product">
                            <input type="checkbox" class="form-check-input delete-checkbox" name="id[]"
                                   value="<?= $product['id'] ?>">
                            <p><?= $product['sku'] ?></p>
                            <p><?= $product['name'] ?></p>
                            <p><?= $product['price'] ?> $</p>
                            <?php
                            $properties = $model->getPropertiesByProductID($product['id']);
                            foreach ($properties as $property): ?>
                                <p><?= $property['name'] ?>: <?= $property['value'] ?> <?= $property['units'] ?></p>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </form>
    </div>
</div>