<?php
/** @var $model \app\models\ProductAddForm */

$this->title = 'Product Add';
?>
<div class="row mt-3">
    <div class="col-12">
        <div class="row align-items-center mx-1">
            <div class="col">
                <h1><?= $this->title ?></h1>
            </div>
            <div class="col-auto">
                <div class="buttons">
                    <button type="submit" form="product_form" class="btn btn-success">Save</button>
                    <a href="/" class="d-inline-block">
                        <button class="btn btn-danger">Cancel</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <hr>
    </div>
    <div class="col-lg-4 col-md-6 col-12">
        <form action="" method="POST" id="product_form"
              class="needs-validation" novalidate>
            <div class="row">
                <div class="col">
                    <div class="form-group mb-3">
                        <label for="sku" class="form-label">SKU</label>
                        <input type="text" class="form-control" id="sku" name="sku" required>
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="form-group mb-3">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="form-group mb-3">
                        <label for="price" class="form-label">Price ($)</label>
                        <input type="text" class="form-control" id="price" name="price" required>
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="form-group mb-3">
                        <label for="productType" class="form-label">Type switcher</label>
                        <select class="form-select" id="productType" name="category_id" required>
                            <option value="0" selected disabled></option>
                            <option value="1">DVD</option>
                            <option value="2">Book</option>
                            <option value="3">Furniture</option>
                        </select>
                        <div class="invalid-feedback"></div>
                    </div>
                </div>
            </div>
            <div class="row product-type category-1 collapse">
                <div class="col">
                    <div class="form-group mb-3">
                        <label for="size" class="form-label">Size (MB)</label>
                        <input type="text" class="form-control" id="size" name="size">
                    </div>
                </div>
                <div class="mb-3">
                    <p class="fw-bold">Please, provide size</p>
                </div>
            </div>
            <div class="row product-type category-2 collapse">
                <div class="col">
                    <div class="form-group mb-3">
                        <label for="weight" class="form-label">Weight (KG)</label>
                        <input type="text" class="form-control" id="weight" name="weight">
                    </div>
                </div>
                <div class="mb-3">
                    <p class="fw-bold">Please, provide weight</p>
                </div>
            </div>
            <div class="row product-type category-3 collapse">
                <div class="col">
                    <div class="form-group mb-3">
                        <label for="length" class="form-label">Length (CM)</label>
                        <input type="text" class="form-control" id="length" name="length">
                    </div>
                    <div class="form-group mb-3">
                        <label for="width" class="form-label">Width (CM)</label>
                        <input type="text" class="form-control" id="width" name="width">
                    </div>
                    <div class="form-group mb-3">
                        <label for="height" class="form-label">Height (CM)</label>
                        <input type="text" class="form-control" id="height" name="height">
                    </div>
                    <div class="mb-3">
                        <p class="fw-bold">Please, provide dimensions</p>
                    </div>
                </div>
            </div>
            <div class="row form-exception collapse">
                <div class="col">
                    <div class="mb-3">
                        <p class="message fw-bold"></p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>