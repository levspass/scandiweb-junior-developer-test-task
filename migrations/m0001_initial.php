<?php


use \app\core\Application;


class m0001_initial
{
    public function up()
    {
        $db = Application::$app->db;
        $SQL = "
            CREATE TABLE IF NOT EXISTS `products` (
                `id` int AUTO_INCREMENT PRIMARY KEY,
                `sku` varchar(255) NOT NULL,
                `name` varchar(255) NOT NULL,
                `price` decimal(11,2) UNSIGNED NOT NULL,
                `category_id` int UNSIGNED NOT NULL
            ) ENGINE=InnoDB;

            CREATE TABLE IF NOT EXISTS `categories` (
                `id` int AUTO_INCREMENT PRIMARY KEY,
                `name` varchar(255) NOT NULL
            ) ENGINE=InnoDB;

            INSERT INTO `categories` (`id`, `name`) VALUES
                (1, 'DVD-disc'),
                (2, 'Book'),
                (3, 'Furniture');

            CREATE TABLE IF NOT EXISTS `properties` (
                `id` int AUTO_INCREMENT PRIMARY KEY,
                `name` varchar(255) NOT NULL,
                `units` varchar(255) NOT NULL,
                `category_id` int UNSIGNED NOT NULL
            ) ENGINE=InnoDB;

            INSERT INTO `properties` (`id`, `name`, `category_id`) VALUES
                (1, 'Size', 1),
                (2, 'Weight', 2),
                (3, 'Dimensions', 3);

            CREATE TABLE IF NOT EXISTS `product_properties` (
                `id` int AUTO_INCREMENT PRIMARY KEY,
                `value` text NOT NULL,
                `property_id` int UNSIGNED NOT NULL,
                `product_id` int UNSIGNED NOT NULL
            ) ENGINE=InnoDB;
        ";
        $db->pdo->exec($SQL);
    }

    public function down()
    {
        $db = \app\core\Application::$app->db;
        $SQL = "
            DROP TABLE IF EXISTS `products`;

            DROP TABLE IF EXISTS `categories`;

            DROP TABLE IF EXISTS `properties`;

            DROP TABLE IF EXISTS `product_properties`;
        ";
        $db->pdo->exec($SQL);
    }
}