<?php

use app\controllers\SiteController;
use app\core\Application;

require_once __DIR__ . '/../vendor/autoload.php';

$dotenv = \Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();
$config = [
    'db' => [
        'dsn' => $_ENV['DB_DSN'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
    ]
];

$app = new Application(dirname(__DIR__), $config);

$app->router->get('/', [SiteController::class, 'productList']);
$app->router->post('/', [SiteController::class, 'productList']);
$app->router->get('/productadd', [SiteController::class, 'productAdd']);
$app->router->post('/productadd', [SiteController::class, 'productAdd']);

$app->run();