$(function () {
    'use strict';

    $('#productType').change(function () {
        let option = $(this).val();
        $('.product-type').each(function () {
            if ($(this).hasClass('category-' + option)) {
                $(this).show();
                $(this).find('input').prop('required', true);
            } else {
                $(this).hide();
                $(this).find('input').prop('required', false);
            }
        });
    });

    $('#product_form').submit(function (e) {
        e.preventDefault();

        $('.form-exception').hide();
        $('.form-exception .message').text('');

        let form = $(this);
        let actionUrl = form.attr('action');

        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data['message']) {
                    $('.form-exception').show();
                    $('.form-exception .message').text(data['message']);
                } else {
                    window.location.replace('/');
                }
            }
        });
    });
});