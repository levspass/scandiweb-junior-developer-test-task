<?php


namespace app\core;


class Response
{
    public function redirect($url)
    {
        header("Location: $url");
    }
}