<?php


namespace app\core\db;

use app\core\Application;
use app\core\Model;


abstract class DbModel extends Model
{
    abstract public static function tableNames(): array;

    public static function prepare($sql): \PDOStatement
    {
        return Application::$app->db->prepare($sql);
    }

    public static function lastInsertId(): string
    {
        return Application::$app->db->pdo->lastInsertId();
    }

    public function getProducts(): array
    {
        $tableNames = static::tableNames();
        $statement = self::prepare("
            SELECT
                id, sku, name, price, category_id
            FROM
                " . $tableNames['products'] . "
            ORDER BY
                id;
        ");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getPropertiesByProductID($id): array
    {
        $tableNames = static::tableNames();
        $statement = self::prepare("
            SELECT
                name, value, units
            FROM
                " . $tableNames['product_properties'] . "
            INNER JOIN
                " . $tableNames['properties'] . "
            ON
                " . $tableNames['product_properties'] . ".property_id = " . $tableNames['properties'] . ".id
            WHERE
                " . $tableNames['product_properties'] . ".product_id = " . $id . ";
        ");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function deleteProducts(): bool
    {
        $tableNames = $this->tableNames();
        $statement = self::prepare("
            DELETE FROM
                " . $tableNames['products'] . "
            WHERE
                id IN (" . implode(', ', $this->id) . ");

            DELETE FROM
                " . $tableNames['product_properties'] . "
            WHERE
                product_id IN (" . implode(', ', $this->id) . ");
        ");
        return $statement->execute();
    }

    public function getSpecificProductClassNameByCategoryID($id): string
    {
        $tableNames = $this->tableNames();
        $statement = self::prepare("
            SELECT
                name
            FROM
                " . $tableNames['categories'] . "
            WHERE
                id = " . $id . ";
        ");
        $statement->execute();
        return $statement->fetchColumn();
    }

    public function insertProduct(): bool
    {
        $tableNames = $this->tableNames();
        $statement = self::prepare("
            INSERT INTO
                " . $tableNames['products'] . "
            SET
                sku = '" . $this->sku . "',
                name = '" . $this->name . "',
                price = '" . $this->price . "',
                category_id = '" . $this->category_id . "';
        ");
        return $statement->execute();
    }

    public function insertProperties(): bool
    {
        $tableNames = $this->tableNames();
        $this->id = self::lastInsertId();
        $properties = self::getPropertiesByCategoryID($this->category_id);
        $statement = '';
        foreach ($properties as $key => $value) {
            if (array_key_exists($value['name'], $this->getProperties())) {
                $statement .= "
                    INSERT INTO
                        " . $tableNames['product_properties'] . "
                    SET
                        property_id = '" . $value['id'] . "',
                        product_id = '" . $this->id . "',
                        value = '" . $this->getProperties()[$value['name']] . "';
                ";
            }
        }
        $statement = self::prepare($statement);
        return $statement->execute();
    }

    public function getPropertiesByCategoryID($id): array
    {
        $tableNames = $this->tableNames();
        $statement = self::prepare("
            SELECT
                id, name, units
            FROM
                " . $tableNames['properties'] . "
            WHERE
                category_id = " . $id . ";
        ");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }
}