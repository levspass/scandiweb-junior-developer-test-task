<?php


namespace app\models;


class ProductListForm extends Product
{
    public array $id = [];

    public function rules(): array
    {
        return [
            'id' => [self::RULE_REQUIRED]
        ];
    }
}