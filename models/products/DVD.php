<?php


namespace app\models\products;

use app\models\Product;


class DVD extends Product
{
    public string $size = '';

    public function rules(): array
    {
        return [
            'size' => [self::RULE_REQUIRED, self::RULE_INT]
        ];
    }

    public function getProperties(): array
    {
        return [
            'Size' => $this->size
        ];
    }

    public function setProperties($request): void
    {
        $this->size = $request['size'];
    }
}