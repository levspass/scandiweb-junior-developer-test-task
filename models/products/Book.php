<?php


namespace app\models\products;

use app\models\Product;


class Book extends Product
{
    public string $weight = '';

    public function rules(): array
    {
        return [
            'weight' => [self::RULE_REQUIRED, self::RULE_INT]
        ];
    }

    public function getProperties(): array
    {
        return [
            'Weight' => $this->weight
        ];
    }

    public function setProperties($request): void
    {
        $this->weight = $request['weight'];
    }
}