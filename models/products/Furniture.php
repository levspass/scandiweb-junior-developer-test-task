<?php


namespace app\models\products;

use app\models\Product;


class Furniture extends Product
{
    public string $length = '';
    public string $width = '';
    public string $height = '';

    public function rules(): array
    {
        return [
            'length' => [self::RULE_REQUIRED, self::RULE_INT],
            'width' => [self::RULE_REQUIRED, self::RULE_INT],
            'height' => [self::RULE_REQUIRED, self::RULE_INT]
        ];
    }

    public function getProperties(): array
    {
        return [
            'Dimensions' => $this->length . 'x' . $this->width . 'x' . $this->height
        ];
    }

    public function setProperties($request): void
    {
        $this->length = $request['length'];
        $this->width = $request['width'];
        $this->height = $request['height'];
    }
}