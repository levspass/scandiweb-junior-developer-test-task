<?php


namespace app\models;


class ProductAddForm extends Product
{
    public int $id = 0;

    public function rules(): array
    {
        return [
            'sku' => [self::RULE_REQUIRED],
            'name' => [self::RULE_REQUIRED],
            'price' => [self::RULE_REQUIRED, self::RULE_INT],
            'category_id' => [self::RULE_REQUIRED]
        ];
    }
}