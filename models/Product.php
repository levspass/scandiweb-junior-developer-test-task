<?php


namespace app\models;

use app\core\db\DbModel;


abstract class Product extends DbModel
{
    public string $sku = '';
    public string $name = '';
    public string $price = '';
    public string $category_id = '';

    public static function tableNames(): array
    {
        return [
            'products' => 'products',
            'categories' => 'categories',
            'properties' => 'properties',
            'product_properties' => 'product_properties'
        ];
    }

    public function getProducts(): array
    {
        return parent::getProducts();
    }

    public function getPropertiesByProductID($id): array
    {
        return parent::getPropertiesByProductID($id);
    }

    public function deleteProducts(): bool
    {
        return parent::deleteProducts();
    }

    public function getSpecificProductObjects()
    {
        return new (__NAMESPACE__ . '\\products\\' . parent::getSpecificProductClassNameByCategoryID($this->category_id));
    }

    public function addProduct(): bool
    {
        return (parent::insertProduct() && parent::insertProperties());
    }
}