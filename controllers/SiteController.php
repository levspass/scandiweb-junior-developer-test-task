<?php


namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\models\ProductAddForm;
use app\models\ProductListForm;


class SiteController extends Controller
{
    public function productList(Request $request)
    {
        $productListForm = new ProductListForm();
        if ($request->isPost()) {
            $productListForm->loadData($request->getBody());
            if ($productListForm->validate() && $productListForm->deleteProducts()) {
                Application::$app->response->redirect('/');
                exit;
            }
        }
        $this->setLayout('main');
        return $this->render('productlist', [
            'model' => $productListForm
        ]);
    }

    public function productAdd(Request $request)
    {
        $productAddForm = new ProductAddForm();
        if ($request->isPost()) {
            $productAddForm->loadData($request->getBody());
            if ($productAddForm->validate()) {
                $product = $productAddForm->getSpecificProductObjects();
                $product->loadData($request->getBody());
                if ($product->validate() && $product->addProduct()) {
                    //Application::$app->response->redirect('/');
                    //exit;
                    echo json_encode(['message' => '']);
                } else {
                    echo json_encode(['message' => $product->getFirstError()]);
                }
            } else {
                echo json_encode(['message' => $productAddForm->getFirstError()]);
            }
            return;
        }
        $this->setLayout('main');
        return $this->render('productadd', [
            'model' => $productAddForm
        ]);
    }
}
